#!/usr/bin/env python
"""
Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
"""

import sys
from AthenaCommon.Logging import log, logging
from AthenaCommon.Constants import DEBUG, VERBOSE, INFO
from AthenaCommon.Configurable import Configurable
from CalypsoConfiguration.AllConfigFlags import ConfigFlags
from AthenaConfiguration.TestDefaults import defaultTestFiles
from CalypsoConfiguration.MainServicesConfig import MainServicesCfg
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
from AthenaPoolCnvSvc.PoolWriteConfig import PoolWriteCfg
from TrackerPrepRawDataFormation.TrackerPrepRawDataFormationConfig import FaserSCT_ClusterizationCfg
from TrackerSegmentFit.TrackerSegmentFitConfig import SegmentFitAlgCfg
from TrackSeedPerformanceWriter.TrackSeedPerformanceWriterConfig import TrackSeedPerformanceWriterCfg

log.setLevel(DEBUG)
Configurable.configurableRun3Behavior = True

ConfigFlags.Input.Files = ['my.RDO.pool.root']
ConfigFlags.Output.ESDFileName = "seeds.ESD.pool.root"
ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-01"
ConfigFlags.IOVDb.DatabaseInstance = "OFLP200"
ConfigFlags.Input.ProjectName = "data21"
ConfigFlags.Input.isMC = True
ConfigFlags.GeoModel.FaserVersion = "FASER-01"
ConfigFlags.Common.isOnline = False
ConfigFlags.GeoModel.Align.Dynamic = False
ConfigFlags.Beam.NumberOfCollisions = 0.
ConfigFlags.lock()

acc = MainServicesCfg(ConfigFlags)
acc.merge(PoolReadCfg(ConfigFlags))
acc.merge(PoolWriteCfg(ConfigFlags))
acc.merge(FaserSCT_ClusterizationCfg(ConfigFlags))
acc.merge(SegmentFitAlgCfg(ConfigFlags, MaxClusters=20, TanThetaCut=0.1))
acc.merge(TrackSeedPerformanceWriterCfg(ConfigFlags))
#acc.getEventAlgo("Tracker::SegmentFitAlg").OutputLevel = DEBUG
acc.getEventAlgo("Tracker::TrackSeedPerformanceWriter").OutputLevel = DEBUG

sc = acc.run(maxEvents=1000)
sys.exit(not sc.isSuccess())
