#ifndef XAODFASERTRACKING_STRIPCLUSTERCONTAINER_H
#define XAODFASERTRACKING_STRIPCLUSTERCONTAINER_H

// Core include(s):
#include "AthContainers/DataVector.h"
// Local include(s):
#include "xAODFaserTracking/StripCluster.h"

namespace xAOD {
    /// The container is a simple typedef for now
    typedef DataVector< xAOD::StripCluster > StripClusterContainer;
}

// Set up a CLID for the container:
#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::StripClusterContainer , 1123887426 , 1 )
 
#endif // XAODFASERTRACKING_STRIPCLUSTERCONTAINER_H