// System include(s):
#include <iostream>

// Local include(s):
#include "xAODFaserTracking/StripRawDataContainer.h"
#include "xAODFaserTracking/StripRawDataAuxContainer.h"

template< typename T >
std::ostream& operator<< ( std::ostream& out,
                           const std::vector< T >& vec ) {

   out << "[";
   for( size_t i = 0; i < vec.size(); ++i ) {
      out << vec[ i ];
      if( i < vec.size() - 1 ) {
         out << ", ";
      }
   }
   out << "]";
   return out;
}

/// Function filling one Strip RDO with information
void fill( xAOD::StripRawData& sc ) {

   sc.setWord( (uint32_t) 1010111);
   sc.setId( (uint64_t) 1111111111111);

   return;
}

/// Function printing the properties of a StripCluster
void print( const xAOD::StripRawData& sc ) {
    
   std::cout << "id = " << sc.id() << std::endl;
   std::cout << "word = " << sc.getWord() << std::endl;

   return;
}

int main() {

   // Create the main containers to test:
   xAOD::StripRawDataAuxContainer aux;
   xAOD::StripRawDataContainer tpc;
   tpc.setStore( &aux );

   // Add one strip cluster to the container:
   xAOD::StripRawData* p = new xAOD::StripRawData();
   tpc.push_back( p );

   // Fill it with information:
   fill( *p );

   // Print the information:
   print( *p );

   // Print the contents of the auxiliary store:
   aux.dump();

   // Return gracefully:
   return 0;
}